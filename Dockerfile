FROM node:6.8.0
MAINTAINER Rade Despodovski <support@doxteam.com>

ADD . /app
RUN cd /app; npm install
EXPOSE 6700
CMD ["node", "/app/server/dst/service.js"]